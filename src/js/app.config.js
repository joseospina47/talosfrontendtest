//JS
import angular from 'angular';
import ngMaterial from 'angular-material';
import uiRouter from 'angular-ui-router';
import highchartsNg from 'highcharts-ng';
import routes from './app.routes';
import services from './app.services';
import controllers from './app.controllers';

//Styles
import '../../node_modules/angular-material/angular-material.min.css';
import '../assets/styles/site.css';

angular.module('TalosDashboard', [
  ngMaterial,
  uiRouter,
  highchartsNg,
  routes.name,
  services.name,
  controllers.name
]);

angular.bootstrap(document, ['TalosDashboard']);
