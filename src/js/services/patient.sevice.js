/**
 * Patient Data Service
 */
class PatientService {

  constructor ($http) {
    this.$http = $http;
  }

  /**
   * Reads the patient information
   */
  getPatientInfo(){
    return this.$http.get('data/PatientData.json')
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return error.data;
      });
  }

  /**
   * Reads the "Current Risk Stratification" information
   */
  getCurRSInfo () {
    return this.$http.get('data/CuRiskStratification.json')
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return error.data;
      });
  }
}

export default PatientService;
