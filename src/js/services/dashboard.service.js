/**
 * Dashboard data service
 */
class DashboardService {

  constructor ($http) {
    this.$http = $http;
  }

  /**
   * Reads the "Overall Financial Summary" Chart Information
   */
  getOSFChartInfo () {
    return this.$http.get('data/OFSChartData.json')
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return error.data;
      });
  }

  /**
   * Reads the "Risk Stratification" table Information
   */
  getRiskStratInfo () {
    return this.$http.get('data/RiskStratification.json')
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return error.data;
      });
  }

  /**
   * Reads the "Physicians Groups" chart Information
   */
  getPGInfo () {
    return this.$http.get('data/PGChartData.json')
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return error.data;
      });
  }

  /**
   * Reads the "Medical Center" Information
   */
  getCenterInfo () {
    return this.$http.get('data/MedicalCenterData.json')
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return error.data;
      });
  }
}

export default DashboardService;
