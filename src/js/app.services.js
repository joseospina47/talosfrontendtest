import DashboardService from './services/dashboard.service';
import PatientService from './services/patient.sevice';

/**
 * Creates a module with all the Services.
 */
export default angular.module('Services', [])
                      .factory('dashboardService',
                        ($http) => new DashboardService($http)
                      )
                      .factory('patientService',
                        ($http) => new PatientService($http)
                      );
