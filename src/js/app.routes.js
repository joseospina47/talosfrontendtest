import dashboard from '../views/dashboard.html';
import patient from '../views/patient.html'

/**
 * Configures the app routes.
 * @param  {Object} $stateProvider
 * @param  {Object} $locationProvider
 */
const config = ($stateProvider, $locationProvider, $urlRouterProvider) => {
  $stateProvider
    .state('dashboard',{
      'url': '/',
      'controller': 'Dashboard as dashboard',
      'template': dashboard
    })
    .state('patient',{
      'url': '/patient',
      'controller': 'Patient as patient',
      'template': patient
    });

  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/');
}

export default angular.module('Routes', [])
                      .config(config);
