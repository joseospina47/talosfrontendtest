import Site from './controllers/site';
import Dashboard from './controllers/dashboard';
import Patient from './controllers/patient';

/**
 * Creates a module with all the controllers.
 */
export default angular.module('Controllers', [])
                      .controller('Site', Site)
                      .controller('Dashboard', Dashboard)
                      .controller('Patient', Patient);
