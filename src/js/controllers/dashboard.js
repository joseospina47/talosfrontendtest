/**
 * Dashboard Section Controller
 */
class Dashboard {

  constructor (dashboardService) {
    this.OFSChartConfig = {};
    this.RSTable = {};
    this.PGChart = {};
    this.medicalCenter = {};
    this.dashboardService = dashboardService;

    this.drawOFSChart();
    this.drawRiskStratTable();
    this.drawPGChart();
    this.getCenterInfo();
  }

  /**
   * Draws the "Overall Financial Summary" Information
   */
  drawOFSChart () {
    this.dashboardService.getOSFChartInfo()
      .then((response) => {
       this.OFSChartConfig = response;
      })
      .catch((error) => {
       console.log('Error drawing the OFS chart: ' + error);
      });
  }

  /**
   * Draws the 'Risk Stratification' table
   */
  drawRiskStratTable () {
    this.dashboardService.getRiskStratInfo()
      .then((response) => {
         this.RSTable = response;
       })
       .catch((error) => {
         console.log('Error drawing the Risk Stratification chart: ' + error);
       });
  }

  /**
   * Draws the "Physicias Groups" chart
   */
  drawPGChart () {
    this.dashboardService.getPGInfo()
      .then((response) => {
         this.PGChart = response;
       })
       .catch((error) => {
         console.log('Error drawing the Risk Stratification chart: ' + error);
       });
  }

  /**
   * Retrieves the medical center information
   */
  getCenterInfo () {
    this.dashboardService.getCenterInfo()
      .then((response) => {
         this.medicalCenter = response;
       })
       .catch((error) => {
         console.log('Error retrieving the midacal center info: ' + error);
       });
  }
}

export default Dashboard;
