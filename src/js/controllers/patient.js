/**
 * Patient Section Controller
 */
class Patient {

  constructor (patientService) {
    this.patientService = patientService;
    this.patientInfo = {};
    this.curRSInfo = {};

    this.getPatientInfo();
    this.getCurRSInfo();
  }

  /**
   * Retrieves the Patient information
   */
  getPatientInfo () {
    this.patientService.getPatientInfo()
      .then((response) => {
         this.patientInfo = response;
       })
       .catch((error) => {
         console.log('Error retrieving the midacal center info: ' + error);
       });
  }

  /**
   * Retrieves the "Current Risk Stratification" information
   */
  getCurRSInfo () {
    this.patientService.getCurRSInfo()
      .then((response) => {
         this.curRSInfo = response;
       })
       .catch((error) => {
         console.log('Error retrieving the midacal center info: ' + error);
       });
  }

}

export default Patient;
