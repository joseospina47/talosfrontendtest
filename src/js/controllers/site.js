/**
 * Global Controller
 */
class Site {

  constructor ($mdSidenav, $window, $location) {
    this.loggedUser = 'Dr. Ospina';
    this.$mdSidenav = $mdSidenav;
    this.$window    = $window;
    this.$location  = $location;
  }

  /**
   * Toggles the left menu
   */
  toggleMenu () {
    this.$mdSidenav('right').toggle();
  }

  /**
   * Redirects the browser to the talos webpage.
   */
  goHome () {
    this.$window.open('http://www.talosdigital.com/', '_blank');
  }

  /**
   * Redirects the app to the dashboard route
   */
  goToDashboard () {
    this.$location.path('/');
    this.toggleMenu();
  }

  /**
   * Redirects the app to the patient route
   */
  goToPatient () {
    this.$location.path('/patient');
    this.toggleMenu();
  }
}

export default Site;
